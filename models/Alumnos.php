<?php
namespace app\models;

class Alumnos extends \yii\base\Model{
    public int $id;
    public string $nombre;
    public string $curso;
    
    public function rules(): array {
        return [
            [['id','nombre','curso'],'required'],
            [['nombre'],'string'],
            [['id'],'int'],
        ];
    }
    
    public function attributeLabels(): array {
        return [
            'id' => 'Codigo del Alumno',
            'nombre' => 'Nombre',
            'curso' => 'Ultimo Curso realizado',
        ];
    }


    
}
