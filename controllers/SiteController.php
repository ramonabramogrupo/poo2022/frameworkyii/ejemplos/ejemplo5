<?php

namespace app\controllers;

use app\models\Alumno;
use app\models\Alumnos;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use const YII_ENV_TEST;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    /**
     * accion que me permite mostrar un array enumerado de
     * arrays asociativos utilizando Html::ul o Html::ol o foreach
     * 
     */
    public function actionEjercicio1() {
        
        // array enumerado
        // de arrays asociativos
        $alumnos=[
            [
                "id" => 1,
                "nombre" => "Eva",
                "curso" => "Excel"
            ],
            [
                "id" => 2,
                "nombre" => "Luis",
                "curso" => "Access"
            ],
            [
                "id" => 3,
                "nombre" => "Susana",
                "curso" => "Excel"
            ],
        ];
        
                
        // para mostrar el array asociativo lo mando a la vista
        // ejercicio1
        return $this->render("ejercicio1",[
            "datos" => $alumnos,
        ]);
        
    }
    
    // mostrar el contenido de un array enumerado
    // utilizando Html::ul o Html::ol o foreach
    
    public function actionEjercicio2() {
        $numeros=[
            1,"23",3,4,5,6,7,8
        ];
        
        return $this->render("ejercicio2",[
            "datos" => $numeros
        ]);
    }
    
    /*
     * esta accion me permite mostrar el contenido
     * de dos arrays enumerados de arrays asociativos
     * utilizando en la vista Html::ul, Html::ol , foreach
     */
    
    public function actionEjercicio3(){
        $aprobados=[
           [
                "id" => 1,
                "nombre" => "Eva",
                "curso" => "Excel"
            ],
            [
                "id" => 2,
                "nombre" => "Luis",
                "curso" => "Access"
            ],
        ];
        
        $suspensos=[
            [
                "id" => 3,
                "nombre" => "Susana",
                "curso" => "Excel"
            ],   
        ];
        
        return $this->render("ejercicio3",[
            "aprobados" => $aprobados,
            "suspensos" => $suspensos
        ]);
    }
    
    /**
     * Quiero mostrar el contenido de un array enumerado
     * de arrays asociativos pero utilizando en la vista
     * el widget GRIDVIEW
     */
    
    public function actionEjercicio4(){
      
              
        // array de arrays asociativos
        $alumnos=[
            [
                "id" => 1,
                "nombre" => "Eva",
                "curso" => "Excel",
            ],
            [
                "id" => 2,
                "nombre" => "Luis",
                "curso" => "Access"
            ],
            [
                "id" => 3,
                "nombre" => "Susana",
                "curso" => "Excel"
            ],
        ];
        
        // convierto el array en un proveedor de datos
        $dataProvider=new ArrayDataProvider([
            "allModels" => $alumnos
        ]);
        
        // le mando el proveedor de datos a la vista
        // y la vista utilizando un gridview me muestra todos los registros
        return $this->render("ejercicio4",[
           "dataProvider" => $dataProvider 
        ]);
    }
    
    // tengo un array de modelos
    // que quiero visualizar utilizando un GRIDVIEW
    
    public function actionEjercicio5(){
        
        // array modelos
        $alumnos=[
            new Alumnos([
                "id" => 1,
                "nombre" => "Eva",
                "curso" => "Excel"
            ]),
            new Alumnos([
                "id" => 2,
                "nombre" => "Luis",
                "curso" => "Access"
            ]),
            new Alumnos([
                "id" => 3,
                "nombre" => "Susana",
                "curso" => "Excel"  
            ]),
        ];
       
        
        //aqui muestro los alumnos con un gridview
        $dataProvider=new ArrayDataProvider([
            "allModels" => $alumnos
        ]);
        
        return $this->render("ejercicio4",[
           "dataProvider" => $dataProvider 
        ]);
        
    }
    
    // TENGO UN ARRAY DE MODELOS Y QUIERO VISULIZARLO CON UN LISTVIEW
    
    public function actionEjercicio6(){
        
        // array modelos
        $alumnos=[
            new Alumnos([
                "id" => 1,
                "nombre" => "Eva",
                "curso" => "Excel"
            ]),
            new Alumnos([
                "id" => 2,
                "nombre" => "Luis",
                "curso" => "Access"
            ]),
            new Alumnos([
                "id" => 3,
                "nombre" => "Susana",
                "curso" => "Excel"  
            ]),
        ];
                
        // CREO UN DATAPROVIDER DESDE  EL ARRAY
        $dataProvider=new ArrayDataProvider([
            "allModels" => $alumnos
        ]);
        
        // MANDO EL DATAPROVIDER A LA VISTA
        return $this->render("ejercicio6",[
           "dataProvider" => $dataProvider 
        ]);
        
    }
    
    
    public function actionEjercicio7(){
        //quiero consultar la tabla Alumno
        
        // consulta sin ejecutar
        // me devuelve un ActiveQuery
        $query=Alumno::find(); 
        
        $dataProvider=new ActiveDataProvider([
            "query" => $query
        ]);
        
        // mandarle el resultado de la query
        // a una vista
        
        return $this->render("ejercicio7",[
            "dataProvider" => $dataProvider
        ]);
    }
    
     public function actionEjercicio8(){
        //quiero consultar la tabla Alumno
        
        // consulta sin ejecutar
        // me devuelve un array de ActiveRecord (modelos)
        $resultado=Alumno::find()->all(); 
        
        $dataProvider=new ArrayDataProvider([
            "allModels" => $resultado
        ]);       
        
        
        // mandarle el resultado de la query
        // a una vista
        
        return $this->render("ejercicio7",[
            "dataProvider" => $dataProvider
        ]);
    }
    
    
    public function actionEjercicio9(){
        
        // creo un modelo Alumnos
        $model=new Alumnos();
        $model->id=10;
        $model->nombre="Federico";
        $model->curso="Word";
        
        // mando el modelo a la vista
        // y en la vista lo visualizo con
        // detailView
        return $this->render("ejercicio9",[
            "model" => $model
        ]);
        
    }
    
    public function actionEjercicio10(){
        // creo un registro
        // con un array asociativo
        $dato=[
            "id" => 10,
            "nombre" => "Federico",
            "curso" => "Word"
        ];
        
        // mando el array asociativo
        // a una vista que tiene el detailView
        return $this->render("ejercicio9",[
            "model" => $dato
        ]);
        
    }
    
    public function actionEjercicio11(){
        // creando una clase basado en ActiveRecord
        $model=new Alumno();
        $model->id=10;
        $model->nombre="Federico";
        $model->curso="Word";
        
        $model->save(); // inserto o actualizo el registro en la tabla
        
        // mando el modelo a la vista
        // y en la vista lo visualizo con
        // detailView
        return $this->render("ejercicio9",[
            "model" => $model
        ]);
        
    }
    
    public function actionEjercicio12($id=1){
        // creando una clase basado en ActiveRecord
        // ejecutando la consulta de sql
        // select * from alumno where id=1
        $model=Alumno::findOne($id);
               
                
        // mando el modelo a la vista
        // y en la vista lo visualizo con
        // detailView
        return $this->render("ejercicio9",[
            "model" => $model
        ]);
        
    }
    
    public function actionEjercicio13(){
        // array con el contenido del directorio actual
        $contenido= scandir(".");
             
        
        $dataProvider=new ArrayDataProvider([
           "allModels" => $contenido 
        ]);
        
        return $this->render("ejercicio13",[
            "contenido" => $contenido,
            "dataProvider" => $dataProvider
        ]);
    }
    
}

