<?php

use yii\helpers\Html;

// mostrar los datos con Html helper
echo Html::ul($datos);

// mostrar los datos con foreach
foreach($datos as $valor){
    echo "<div>{$valor}</div>";
}